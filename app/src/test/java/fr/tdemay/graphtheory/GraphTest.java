package fr.tdemay.graphtheory;

import org.junit.Test;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import fr.tdemay.graphtheory.GraphTheoryClasses.Arc;
import fr.tdemay.graphtheory.GraphTheoryClasses.Graph;
import fr.tdemay.graphtheory.GraphTheoryClasses.Node;

import static org.junit.Assert.*;


public class GraphTest {
    @Test
    // Testing the validity of the adjacent matrix generation
    public void adjMXGeneratorTest() {

        String data[][] = {{"A", "A", "1"},
                {"A", "B", "1"},
                {"A", "C", "1"},
                {"B", "C", "1"},
                {"B", "D", "1"},
                {"C", "B", "1"},
                {"C", "D", "1"},
                {"D", "C", "1"},
                {"D", "D", "1"}};

       Graph myGraph = new Graph(data);
        // Un problème connu

        int[][] answer = {
                {1, 1, 1, 0},
                {0, 0, 1, 1},
                {0, 1, 0, 1},
                {0, 0, 1, 1}
        };
        // test the equivalence of each cell of two multi-dimensionnal arrays
        assertEquals(true, Arrays.deepEquals(myGraph.getAdjMX(),answer));
    }

    // Testing the unvalidation of a graph with empty nodes and arcs
    @Test(expected = IllegalArgumentException.class)
    public void graphWrongSizedDataArray() {
        String data[][] = new String [5][1];
        Graph myGraph = new Graph(data);
    }


    // endregion
}