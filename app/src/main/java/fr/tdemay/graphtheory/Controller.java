package fr.tdemay.graphtheory;

import android.view.View;
import android.widget.EditText;
import java.util.LinkedList;
import fr.tdemay.graphtheory.GraphTheoryClasses.Graph;

public class Controller {
    private Graph graph;

    //region Constructeur
    public Controller() {
    }

    //endregion
    public void graphGenerate(String data[][]) {
        try {
            this.graph = new Graph(data);
        }
        catch (Exception e) {

        }
    }

    public String[][] arcsMxGenerate(LinkedList<View[]> views) {
        String[][] mx = new String[views.size()][3];
        int i = 0;
        for (View[] view : views) {
            mx[i][0] = ((EditText) view[0]).getText().toString();
            mx[i][1] = ((EditText) view[1]).getText().toString();
            //TODO : à modifier pour récupérer la valeur de l'arc si nécessaire
            mx[i][2] = "1";
            i++;
        }
        return mx;
    }

    //region Getters
    public int[][] GetAdjMx() {
        return this.graph.getAdjMX();
    }

    public Graph getGraph() {
        return graph;
    }
//endregion

    //region Checker
    //a method to check if the inputs arcs are not redundant
    //TODO : to be developed in order to avoid crash during validation of inputs by user
    public boolean isArcsInputValid(LinkedList<View[]> inputs) {
        LinkedList<String> arcNames = new LinkedList<String>();
        for (View input[] : inputs
        ) {
            String name = ((EditText) input[0]).getText().toString() + ((EditText) input[1]).getText().toString();
            if (arcNames.contains(name)) {
                return false;
            } else {
                arcNames.add(name);
            }
        }
        return true;
    }
    //endregion
}
