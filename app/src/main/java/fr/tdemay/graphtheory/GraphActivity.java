package fr.tdemay.graphtheory;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class GraphActivity extends AppCompatActivity {

    private Controller controller;
    private TableLayout ui_tableLayout;
    private TextView ui_lbl_graph;
    private TextView ui_lbl_nodes_title;
    private TextView ui_lbl_nodes_info;
    private TextView ui_lbl_arcs_title;
    private TextView ui_lbl_arcs_info;
    private TextView ui_lbl_adjMx_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        this.controller = new Controller();
        // graph generation
        this.controller.graphGenerate((String[][]) getIntent().getExtras().get("data"));
        this.ui_tableLayout = findViewById(R.id.ag_tableLayout);
        loadData(this.controller.getGraph().getAdjMX(), this.ui_tableLayout);
        updateLabels();
    }

    // a function to finding and filling the textviews
    public void updateLabels() {
        this.ui_lbl_graph = findViewById(R.id.ag_lbl_graph);
        this.ui_lbl_nodes_title = findViewById(R.id.ag_lbl_nodes_title);
        this.ui_lbl_nodes_info = findViewById(R.id.ag_lbl_nodes_info);
        this.ui_lbl_arcs_title = findViewById(R.id.ag_lbl_arcs_title);
        this.ui_lbl_arcs_info = findViewById(R.id.ag_lbl_arcs_info);
        this.ui_lbl_adjMx_title = findViewById(R.id.ag_lbl_adjMx_title);
        this.ui_lbl_graph.setText(R.string.your_graph);
        this.ui_lbl_nodes_title.setText(getString(R.string.nbNodestitle, this.controller.getGraph().getNodes().size()));
        this.ui_lbl_nodes_info.setText(this.controller.getGraph().getNodesInfo());
        this.ui_lbl_arcs_title.setText(getString(R.string.nbArcstitle, this.controller.getGraph().getArcs().size()));
        this.ui_lbl_arcs_info.setText(this.controller.getGraph().getArcsInfo());
        this.ui_lbl_adjMx_title.setText(R.string.mxAdjtitle);
    }



    // a fonction filling a table layout with a two dimensions array of integers
    public void loadData(int[][] data, TableLayout tableLayout) {

        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize = 0, mediumTextSize = 0;

        textSize = (int) getResources().getDimension(R.dimen.font_size_verysmall);
        smallTextSize = (int) getResources().getDimension(R.dimen.font_size_small);
        mediumTextSize = (int) getResources().getDimension(R.dimen.font_size_medium);


        int nbcolumn = data.length;

        TextView textSpacer = null;

        tableLayout.removeAllViews();

        // for each row in the mx
        // -1 means heading row
        for (int i = -1; i < nbcolumn; i++) {
            int[] row = null;
            if (i > -1)
                row = data[i];
            else {
                textSpacer = new TextView(this);
                textSpacer.setText("");

            }

            // create a new table row
            final TableRow tr = new TableRow(this);
            tr.setId(i + 1);
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0, 0, 0, 0);
            tr.setLayoutParams(trParams);


            // for each column, create a text view

            for (int j = 0; j < nbcolumn; j++) {
                final TextView tv = new TextView(this);
                tv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT));

                tv.setGravity(Gravity.LEFT);

                tv.setPadding(5, 15, 0, 15);
                if (i == -1) {
                    // first row is filled with the name of the node
                    tv.setText(this.controller.getGraph().getNodes().get(j).getName());
                    tv.setBackgroundColor(Color.parseColor("#f0f0f0"));
                    tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
                } else {
                    tv.setBackgroundColor(Color.parseColor("#f8f8f8"));
                    tv.setText(Integer.toString(row[j]));
                    tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                }
                // add each textview in the row
                tr.addView(tv);
            }
            // add each row in the tableLayout
            ui_tableLayout.addView(tr, trParams);
        }

// Original tutorial on http://truelogic.org/wordpress/2016/05/01/showing-dynamic-data-in-a-tablelayout-in-android/

    }
}


