package fr.tdemay.graphtheory.GraphTheoryClasses;


import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Graph {
    private ArrayList<Node> nodes;
    private ArrayList<Arc> arcs;
    private int[][] adjMX;
    private String adjMXTOSTRING = "";
    private HashMap<Integer, ArrayList<Node>> mapNodes;


    //region Constructor
    // Constructor
    public Graph(String data[][]) {
        this.nodes = new ArrayList<Node>();
        this.arcs = new ArrayList<Arc>();
        this.mapNodes = new HashMap<Integer, ArrayList<Node>>();
        String node1name;
        String node2name;
        int value;
        for (int i = 0; i < data.length; i++) {
            // Exception if data are not a table [X][3]
            if (data[i].length != 2 && data[i].length != 3) {
                throw new IllegalArgumentException("Graph creation : data format incorrect ");
            } else {
                node1name = data[i][0].toString();
                node2name = data[i][1].toString();
                if (data[i].length == 3) {
                    value = Integer.parseInt(data[i][2].toString());
                } else {
                    value = 1;
                }
                this.getNode(node1name);
                this.getNode(node2name);
                this.getArc(node1name, node2name, value);
            }
        }
        this.adjMXGenerator();
        this.orderingNodes();
    }


    // To add a new node to the Graph
    private void addNode(Node nodeNew) {
        try {
            // check if not null
            if (nodeNew == null) {
                throw new Exception("This node is null !");
            } else {
                // a boolean checking if the input name is free
                boolean isFreeName = true;
                //checking for each node in the list if the name is already taken
                for (Node node : this.nodes) {

                    if ((nodeNew.getName() != node.getName()) && (isFreeName)) {
                        isFreeName = true;
                    } else isFreeName = false;
                }
                if (isFreeName) {
                    this.nodes.add(nodeNew);
                    // attributing a position corresponding to the line/column in the adj Mx
                    nodeNew.setPosition(this.nodes.size() - 1);

                } else {
                    throw new Exception("This name is already taken by another node of the list ! ");
                }
            }

        } catch (
                Exception e) {

        }

    }

    // To add a new arc
    private void addArc(Arc arcNew) {
        for (Arc arc : this.arcs) {
            if (arc.getName().equals(arcNew.getName())) {
                throw new IllegalArgumentException("Arc Creation : There is already an arc using these nodes");
            }
        }
        this.arcs.add(arcNew);
    }


    //endregion

    // region Getters and Setters

    private Node getNode(String nodeName) {
        Node nodetoret;
        for (Node node : this.nodes) {
            if (node.getName().equals(nodeName)) {
                return node;
            }
        }
        nodetoret = new Node(nodeName);
        this.addNode(nodetoret);
        return nodetoret;
    }

    // Give an arc from the graph if it does exist, or create it and add it to the graph
    private Arc getArc(String node1name, String node2name, int value) {
        Arc arctoret;
        for (Arc arc : this.arcs) {
            if (arc.getName().equals(node1name.concat(node2name))) {
                return arc;
            }
        }
        Node node1 = getNode(node1name);
        Node node2 = getNode(node2name);
        arctoret = new Arc(node1, node2, value);
        node1.getSucessors().add(node2);
        node2.getPredecessors().add(node1);
        this.addArc(arctoret);
        return arctoret;
    }

    public List<Node> getNodes() {
        return Collections.unmodifiableList(nodes);
    }

    public List<Arc> getArcs() {
        return Collections.unmodifiableList(arcs);
    }

    public int[][] getAdjMX() {
        return adjMX;
    }


    public HashMap<Integer, ArrayList<Node>> getMapNodes() {
        return mapNodes;
    }

    public void setMapNodes(HashMap<Integer, ArrayList<Node>> mapNodes) {
        this.mapNodes = mapNodes;
    }


    //endregion

    //region Maths Functions

    // Method to generate the adjacent matrix using nodes and arcs lists
    public void adjMXGenerator() {

        if (this.arcs.isEmpty() || this.nodes.isEmpty()) {
            throw new IllegalArgumentException("adjMXGenerator : No nodes and/or no arcs.");
        } else {
            this.adjMX = new int[nodes.size()][nodes.size()];

            for (Arc arc : this.arcs) {
                // filling matrix at positions of nodes with the value
                this.adjMX[arc.getNode1().getPosition()][arc.getNode2().getPosition()] = arc.getValue();
            }
        }
    }

    // method to order the nodes and fill the mapNodes with level
    public void orderingNodes() {
        // all the nodes from the graph
        ArrayList<Node> toBeOrderedNodes = new ArrayList<Node>();
        toBeOrderedNodes.addAll(this.getNodes());
        // a list to stock the node to order at a specific level
        ArrayList<Node> waitingToBeOrderedNodes = new ArrayList<Node>();
        // the list of already ordered nodes
        ArrayList<Node> orderedNodes = new ArrayList<Node>();
        int currentLevel = 0;
        // creation of the level 0 in the mapNodes
        this.getMapNodes().put(currentLevel, new ArrayList<Node>());
        // boolean to validate the level assignation
        boolean isOnThisLevel = true;

        // double condition while : less than 15 level for a graph and there is still some nodes to order
        while (currentLevel < 15 && !toBeOrderedNodes.isEmpty()) {
            // 1 LVL => 1 LOOP. Starting by adding a new level on the map
            this.getMapNodes().put(currentLevel, new ArrayList<Node>());
            // foreach on all nodes to be treated
            for (Node node : this.getNodes()) {

                // if the nodes is not already ordered
                if (!orderedNodes.contains(node)) {
                    // boolean is reassign on true by default
                    isOnThisLevel = true;
                    // specific case of first level : if there are predecessors, it is not level 0 node
                    if (currentLevel == 0) {
                        if (!node.getPredecessors().isEmpty()) {
                            // pred on level 0 are not possible
                            isOnThisLevel = false;
                        }
                    }
                    // for level 1 and next
                    else {
                        // for all the predecessors of the current node
                        for (Node preNode : node.getPredecessors()) {
                            // if this pred is not in the already ordered nodes and its not itself(loop node)
                            if (!orderedNodes.contains(preNode)&& !preNode.equals(node)) {
                                isOnThisLevel = false;
                            }
                        }
                    }


                    // if the node is ok with this level
                    if (isOnThisLevel) {
                        toBeOrderedNodes.remove(node);
                        waitingToBeOrderedNodes.add(node);
                        node.setLevel(currentLevel);
                        Objects.requireNonNull(this.getMapNodes().get(currentLevel)).add(node);

                    }
                }
            }
            orderedNodes.addAll(waitingToBeOrderedNodes);
            waitingToBeOrderedNodes.clear();
            currentLevel++;

        }
    }


    //endregion

    //region Debug & Information

    //TODO : remettre les strings node et arc dans strings.xml ? Comment récupérer le contexte ?
    @Override
    public String toString() {
        return "Graph : " + "\n\n" +
                "Nodes :" + "\n" + nodes + "\n\n" +
                "Arcs :" + "\n" + arcs + "\n\n" +
                "AdjMX :" + "\n" + Arrays.deepToString(adjMX);

    }

    public String getNodesInfo() {
        String str = "";
        for (Node node : nodes) {
            str += "Node " + node.getName() + " :  Predecessors : " + node.predInfo() + "| Successors : " + node.succInfo() + "\n";
        }
        return str;
    }

    public String getArcsInfo() {
        String str = "";
        for (Arc arc : arcs) {
            if (arc.getValue() != 1) {
                str += "Arc " + arc.getName() + " = " + arc.getValue() + "\n";
            } else {
                str += "Arc " + arc.getName() + "\n";
            }
        }
        return str;
    }

    public String getMapNodesInfo() {
        return this.mapNodes.toString();
    }
    //endregion
}
