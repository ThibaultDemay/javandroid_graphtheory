package fr.tdemay.graphtheory.GraphTheoryClasses;

public class Arc {

    private Node node1;
    private Node node2;
    private int value;
    private String name;

    //region Getters

    public Node getNode2() {
        return node2;
    }

    public Node getNode1() {
        return node1;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    //endregion

    //region Constructor

    protected Arc(Node node1, Node node2, int value) {

        if (node1 == null || node2 == null) {
            throw new IllegalArgumentException("Arc generation : Both nodes must be not Null.");
        }
       /* if (node1 == node2 || node1.getName().equals(node2.getName())) {
            //TODO : A remettre pour le cas d'un graphe orienté throw new IllegalArgumentException("Arc generation : An arc must link two different nodes.");
        }*/
        else {
            this.node1 = node1;
            this.node2 = node2;
            this.value = value;
            this.name = node1.getName().concat("-"+node2.getName());
        }
    }

    protected Arc(Node node1, Node node2) {
        this(node1, node2, 1);
    }
    //endregion


    @Override
    public String toString() {
        if (value != 1) {
            return "Arc " + name + " // " + node1.getName() + " => " + node2.getName() + " = " + value + '}';
        }
        else {
            return "Arc " + name + " // " + node1.getName() + " => " + node2.getName();
        }
    }
}
