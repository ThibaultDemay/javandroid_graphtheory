package fr.tdemay.graphtheory.GraphTheoryClasses;

import java.util.ArrayList;

public class Node {

    private String name;
    // position in the matrix. No real mathematical sense
    private int position;
    // level of the node in an ordered graph
    private int level;
    private ArrayList<Node> sucessors;
    private ArrayList<Node> predecessors;

    //region Getters and Setters
    public String getName() {
        return name;
    }

    public int getPosition() {
        return position;
    }

    protected void setPosition(int position) {
        this.position = position;
    }

    public ArrayList<Node> getSucessors() {
        return sucessors;
    }

    public ArrayList<Node> getPredecessors() {
        return predecessors;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
//endregion

    //region Constructor
    public Node(String name) {
        this.name = name;
        this.position = 0;
        this.sucessors = new ArrayList<Node>();
        this.predecessors = new ArrayList<Node>();

    }
    //endregion

    //region Debug & Information
    @Override
    public String toString() {
        return "Node " + name + " : Position = " + position + ". Predecessors : " + this.predInfo() + ". Successors : " + this.succInfo() + ". Level : " + this.getLevel();
    }

    public String predInfo() {
        String str = new String();
        if (predecessors.isEmpty()) {
            str = "None ";
            return str;
        }
        for (Node n : predecessors) {
            str += n.getName() + " ";
        }
        return str;
    }

    public String succInfo() {
        String str = new String();
        if (sucessors.isEmpty()) {
            str = "None ";
            return str;
        }
        for (Node n : sucessors) {
            str += n.getName() + " ";
        }
        return str;
    }
//endregion
}
