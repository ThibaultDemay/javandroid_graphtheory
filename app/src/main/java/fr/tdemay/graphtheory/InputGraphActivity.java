package fr.tdemay.graphtheory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

public class InputGraphActivity extends AppCompatActivity {

    private Controller controller;
    private LinearLayout ui_scrollView_linearLayout;
    private LinkedList<View[]> inputsList;
    private TextView ui_lbl_title;
    private TextView ui_lbl_subtitle;
    private int nbArcsInputs = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_graph);
        this.controller = new Controller();
        inputsList = new LinkedList<View[]>();
        this.ui_scrollView_linearLayout = findViewById(R.id.aig_SV_LL);

        // adding a first arc input view

        addArcInput();
        updateLabels();
    }

    // Click function for adding new arc input view
    public void newArcInputClick(View view) {
        addArcInput();
    }

    // function adding a new arc
    private void addArcInput() {
        nbArcsInputs++;
        // getting inflater and constraint layout inside the scrollview
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // creating a view using the template input_arc_cell
        View custom = inflater.inflate(R.layout.input_arc_cell, null);

        // filling the texts
        EditText inputNode1 = (EditText) custom.findViewById(R.id.arc_cel_inp_node1);
        EditText inputNode2 = (EditText) custom.findViewById(R.id.arc_cel_inp_node2);
        TextView lblArc = custom.findViewById(R.id.arc_cel_lbl);

        //calculation of the new letters used by default
        char node1char = (char) (65 + ((2 * nbArcsInputs) / 26) * 6 + nbArcsInputs  - 1);
        char node2char = (char) (65 + ((2 * nbArcsInputs) / 26) * 6 + nbArcsInputs);

        inputNode1.setText(Character.toString(node1char));
        inputNode2.setText(Character.toString(node2char));
        lblArc.setText(String.format(getResources().getString(R.string.arcNb), Integer.toString(nbArcsInputs)));
        // adding the view to the constraintlayout in the scrollview
        this.ui_scrollView_linearLayout.addView(custom);

        // adding inputsNode inside a list
        View[] inputsArc = {inputNode1, inputNode2};
        this.inputsList.add(inputsArc);
    }

    //finding and feeling the textview
    private void updateLabels() {
        ui_lbl_title = findViewById(R.id.aig_lbl_title);
        ui_lbl_subtitle = findViewById(R.id.aig_lbl_subtitle);
        ui_lbl_title.setText(R.string.inputtitle);
        ui_lbl_subtitle.setText(R.string.inputsubtitle);
    }

    // function on click on the validation button
    public void graphCreationClick(View view) {
        if ((Boolean) this.controller.isArcsInputValid(inputsList)) {
            Intent intent = new Intent(InputGraphActivity.this, GraphActivity.class);
            //String[][] data = this.controller.arcsMxGenerate(inputsList);
            // getting the mx of string from arcInputList, and putExtra into intent
            intent.putExtra("data", this.controller.arcsMxGenerate(inputsList));
            InputGraphActivity.this.startActivity(intent);
        } else {
            TextView ui_lbl_info = findViewById(R.id.aig_lbl_info);
            ui_lbl_info.setText(R.string.inputinfoNOK);
        }
    }
}
